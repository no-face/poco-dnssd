from conans import ConanFile, tools

import sys
import inspect

from os import path

# Add parent dir to python path (used to import version while in development)
script_path = path.dirname(path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(1, path.join(script_path, '..'))

import version

class Recipe(ConanFile):
    name = "PocoDNSSD-Bonjour"
    version = version.VERSION
    settings = "os", "compiler", "build_type", "arch"

    build_requires = (
       "waf/0.1.1@noface/stable",
       "WafGenerator/0.1.1@noface/stable",
    )

    generators = "Waf"

    default_options = (
        "Poco:shared=True",
        "Poco:enable_util=False",
        "Poco:enable_xml=False",
        "Poco:enable_json=False",
        "Poco:enable_mongodb=False",
        "Poco:enable_net=True",
        "Poco:enable_netssl=False",
        "Poco:enable_netssl_win=False",
        "Poco:enable_crypto=False",
        "Poco:enable_data=False",
        "Poco:enable_data_sqlite=False",
        "Poco:enable_zip=False",
        "Poco:force_openssl=False",
    )


    exports = "../version.py"
    exports_sources = "wscript", "src/*", "include/*", "../buildtools*"

    # source is not changed by build
    no_copy_source = True

    def requirements(self):
        CORE_VERSION    = tools.get_env("POCODNSSD_VERSION", version.VERSION)
        CORE_USER       = tools.get_env("POCODNSSD_USER", tools.get_env("CONAN_USERNAME", "noface"))
        CORE_CHANNEL    = tools.get_env("POCODNSSD_CHANNEL", tools.get_env("CONAN_CHANNEL", "snapshot"))

        self.requires("PocoDNSSD-core/{}@{}/{}".format(CORE_VERSION, CORE_USER, CORE_CHANNEL))
        self.requires("bonjour/878.30.4@noface/testing")

    def imports(self):
      self.copy("*.dll", dst="bin", src="bin")
      self.copy("*.dylib*", dst="bin", src="lib")

    def build(self):
        build_path = path.join(self.build_folder, "build")

        build_cmd = 'waf configure build -v -o {} {}'.format(build_path, self.get_options())

        self.run_and_log(build_cmd, cwd=self.source_folder)
        self.run_and_log('waf install', cwd=self.source_folder)

    def package(self):
        self.copy("*.h", src=path.join(self.install_dir, "include"), dst="include", keep_path=True)
        self.copy("*", src=path.join(self.install_dir, "lib"), dst="lib", symlinks=True)

    def package_info(self):
        self.cpp_info.libs.append('PocoDNSSDBonjour')
        self.cpp_info.defines.append("HAS_POCO_DNSSD_BONJOUR")

########################################################################################################

    def get_options(self):
        opts = []

        import multiprocessing
        opts.append("-j {}".format(multiprocessing.cpu_count()))

        if self.settings.build_type == "Debug":
           opts.append("--debug")
        else:
           opts.append("--release")

        opts.append("-v")

        opts.append("--prefix=%s" % self.install_dir)

        return " ".join(opts)

    @property
    def install_dir(self):
       return path.join(self.build_folder, "install")

    def run_and_log(self, cmd, cwd=None):
      msg = ''
      if cwd:
          msg = "[{}] ".format(cwd)

      self.output.info(msg + cmd)

      self.run(cmd, cwd = cwd)
