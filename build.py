#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from os import path

from conan.packager import ConanMultiPackager

def main():
    pkg_reference = get_package_reference()
    builder = ConanMultiPackager(
                    reference=pkg_reference,
                    build_types=["Release"])

    builder.add_common_builds()

    if pkg_reference.startswith('PocoDNSSD/'):
        add_build_matrix(builder, [
            {'options': {"PocoDNSSD:use_avahi":False, "PocoDNSSD:use_bonjour":True}},
            {
              'options': {"PocoDNSSD:use_avahi":True, "PocoDNSSD:use_bonjour":False},
              'filter' : is_linux_filter
            },
            {
              'options': {"PocoDNSSD:use_avahi":True, "PocoDNSSD:use_bonjour":True},
              'filter' : is_linux_filter
            }
        ])

    builder.run()

#####################################################################

def add_build_matrix(builder, values_list):
    if len(values_list) == 0:
        return

    builds = []

    for build_items in builder.items:
        settings, options, env_vars, build_requires, reference = build_items

        for values in values_list:
            filter = values.get('filter', None)
            if not build_is_valid(filter, build_items):
                continue

            build = [
                merge_values(values, 'settings'      , settings),
                merge_values(values, 'options'       , options),
                merge_values(values, 'env_vars'      , env_vars),
                build_requires
            ]
            builds.append(build)

    builder.builds = builds

def build_is_valid(filter, build_items):
    return filter is None or filter(*build_items)

def merge_values(values_dict, item_name, default_values):
    values = default_values.copy() if(default_values) else {}

    new_values = values_dict.get(item_name, {})

    for k, v in new_values.items():
        values[k] = v

    return values

def is_linux_filter(settings, *others):
    os_settings = settings.get('os', None)

    if os_settings is not None:
        return os_settings == 'Linux'
    else:
        return sys.platform.startswith('linux')

#####################################################################

def get_package_reference():
    pkg_info = get_package_info()

    return "{}/{}".format(pkg_info[0], pkg_info[1])

def get_package_info():
    import inspect

    conanfile = import_module(path.abspath('conanfile.py'))
    module_members = inspect.getmembers(conanfile, is_recipe)
#    print("module_members({}): {}".format(conanfile, module_members))

    for name, member in module_members:
         if member.__module__ == conanfile.__name__:
             return (member.name, member.version)

    return None

def import_module(module_path):
    import sys
    import imp

    # Change module name to avoid conflict with ConanMultiPackager
    name = 'temp_' + path.splitext(path.basename(module_path))[0]

    sys.dont_write_bytecode = True
    loaded = imp.load_source(name, module_path)
    sys.dont_write_bytecode = False

    return loaded

def is_recipe(member):
    import inspect
    from conans import ConanFile

    return inspect.isclass(member) and issubclass(member, ConanFile)


#####################################################################

if __name__ == "__main__":
    main()
