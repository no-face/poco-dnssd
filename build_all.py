#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import sys
from os import path

def main():
    build_script = path.abspath(path.join('.', 'build.py'))

    for subdir in ['core', 'Avahi', 'Bonjour', '.']:
        cmd = "python {}".format(build_script)
        subprocess.call(cmd.split(), cwd=subdir)

if __name__ == "__main__":
    main()
