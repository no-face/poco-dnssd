#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path

from waflib.Configure import conf

def options(ctx):
    ctx.add_option('--debug', action='store_true', default=True, dest='debug', help='Do debug build')
    ctx.add_option('--release', action='store_false', dest='debug', help='Do release build')

    # Flag to generate shared or static libs
    ctx.add_option('--shared', action='store_true', default=True, dest='shared', help='Build libs as shared libraries')
    ctx.add_option('--static', action='store_false', dest='shared', help='Build libs as static libraries')


def configure(ctx):
    ctx.env.shared      = (ctx.options.shared      == True or ctx.options.shared      == 'True')

    config_default_flags(ctx)

#######################################################################################

def config_default_flags(ctx):
    if ctx.options.debug:
        ctx.env.CXXFLAGS += ['-g']

    ctx.env.CXXFLAGS += ['-Wall']

    if ctx.check_cxx(linkflags="-Wl,--no-undefined"):
        ctx.env.LINKFLAGS.append("-Wl,--no-undefined")

################################### Helpers ###########################################

@conf
def glob(ctx, *k, **kw):
    '''Helper to execute an ant_glob search.
        See documentation at: https://waf.io/apidocs/Node.html?#waflib.Node.Node.ant_glob
    '''

    return ctx.path.ant_glob(*k, **kw)

@conf
def lib(bld, **kw):
    import waflib
    from waflib import Utils
    from waflib.Tools import c_aliases

    if 'install_path' not in kw:
        kw['install_path'] = bld.env.LIBDIR

    features     = Utils.to_list(kw.get("features", ""))
    only_objects = kw.get("only_objects", False)

    objects_kw = kw.copy()

    if not only_objects:
        objects_target = kw.get('target') + ".objects"
        objects_kw['target'] = objects_target

    bld.objects(**objects_kw)

    if not only_objects:
        kw['use']      = [objects_target] + Utils.to_list(kw.get('use', ""))
        #kw['features'] = ["cxx"] + Utils.to_list(kw.get('features', ""))

        lib_type = "shlib" if bld.env.shared else "stlib"
        c_aliases.set_features(kw, lib_type)

        # remove source to not build the same sources again
        kw['source']   = []

        # build library
        bld(**kw)

    includedir = kw.get('install_includedir', bld.env.INCLUDEDIR)

    install_headers = []

    if 'install_headers' in kw:
        install_headers = kw['install_headers']
    else:
        headers_dirs = kw.get('headers_dirs', kw.get('export_includes', []))

        for inc_dir in headers_dirs:

            inc_node = bld.path.make_node(inc_dir)
            incs = inc_node.ant_glob([
                                path.join('**', '*.h'),
                                path.join('**', '*.hpp')])

            install_headers.extend(incs)

    if install_headers:
        headers = Utils.to_list(install_headers)

        headers_base = kw.get("headers_base", None)
        if headers_base and not isinstance(headers_base, waflib.Node.Node):
            headers_base = bld.path.find_dir(headers_base)

        relative_trick = kw.get("install_relative", True)
        bld.install_files(
            includedir,
            headers,
            relative_trick=relative_trick,
            cwd=headers_base)
