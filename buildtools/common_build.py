#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path

def load_tools(ctx):
    ctx.load('compiler_cxx')
    ctx.load('gnu_dirs')

    buildtools_dirs = ["buildtools", path.join("..", 'buildtools')]

    ctx.load('build_lib', tooldir=buildtools_dirs)
    ctx.load('lib_version', tooldir=buildtools_dirs)

def options(ctx):
    load_tools(ctx)

def configure(ctx):
    load_tools(ctx)

    src_dir = ctx.srcnode.abspath()
    ctx.load('conanbuildinfo_waf', tooldir=[src_dir, path.join(ctx.bldnode.abspath(), "..")]);

    config_flags(ctx)


#######################################################################################

def config_flags(ctx):
    ctx.env.CXXFLAGS += ['-std=c++11']

    if ctx.env.shared:
        ctx.env.CXXFLAGS.append('-fPIC')

    if ctx.check_cxx(cxxflags='-pthread', linkflags="-pthread"):
        # set pthread globally to solve pre-compiled header problems
        ctx.env.CXXFLAGS += ['-pthread']
        ctx.env.LINKFLAGS += ['-pthread']

