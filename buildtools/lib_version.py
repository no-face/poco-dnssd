#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import inspect
from os import path

# Add parent dir to python path (used to import version while in development)
script_path = path.dirname(path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(1, path.join(script_path, '..'))

import version

def options(ctx):
    ctx.add_option('--use-soname', action='store_true', default=True, dest='use_soname',
        help="If true, build library with 'soname'")

def configure(ctx):
    ctx.env.use_soname = ctx.options.use_soname
    ctx.env.version_number = version.VERSION if ctx.env.use_soname else None
