from conans import ConanFile, tools
from conans.tools import os_info

class Recipe(ConanFile):
    name = "PocoDNSSD"
    version = "0.1"
    settings = "os", "compiler", "build_type", "arch"

    options = {
        "use_avahi"   : ["default", True, False],
        "use_bonjour" : ["default", True, False]
    }

    default_options = (
        "use_avahi=default",
        "use_bonjour=default"
    )

    def requirements(self):
        user    = tools.get_env("CONAN_USERNAME", "noface")
        channel = tools.get_env("CONAN_CHANNEL", "snapshot")

        if self.use_avahi:
            self.requires("PocoDNSSD-Avahi/0.1@{}/{}".format(user, channel))

        if self.use_bonjour:
            self.requires("PocoDNSSD-Bonjour/0.1@{}/{}".format(user, channel))

########################################################################################################

    @property
    def use_avahi(self):
        if self.options.use_avahi == "default":
            return (self.settings.os == "Linux")

        return self.options.use_avahi == True

    @property
    def use_bonjour(self):
        if self.options.use_bonjour == "default":
           return (self.settings.os != "Linux")

        return self.options.use_bonjour == True
