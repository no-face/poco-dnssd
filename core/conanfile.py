from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.tools import os_info

import sys
import inspect

from os import path

# Add parent dir to python path (used to import version while in development)
script_path = path.dirname(path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(1, path.join(script_path, '..'))

import version

class Recipe(ConanFile):
    name = "PocoDNSSD-core"
    version = version.VERSION
    settings = "os", "compiler", "build_type", "arch"
    requires = "Poco/1.7.9@pocoproject/stable"

    build_requires = (
       "waf/0.1.1@noface/stable",
       "WafGenerator/0.1.1@noface/stable",
    )

    generators = "Waf"

    default_options = (
        "Poco:shared=True",
        "Poco:enable_util=False",
        "Poco:enable_xml=False",
        "Poco:enable_json=False",
        "Poco:enable_mongodb=False",
        "Poco:enable_net=True",
        "Poco:enable_netssl=False",
        "Poco:enable_netssl_win=False",
        "Poco:enable_crypto=False",
        "Poco:enable_data=False",
        "Poco:enable_data_sqlite=False",
        "Poco:enable_zip=False",
        "Poco:force_openssl=False",
#
#       Poco:use_pic=True
    )

    exports = "../version.py"
    exports_sources = "wscript", "src/*", "include/*", "../buildtools*"

    # source is not changed by build
    no_copy_source = True

    def imports(self):
      self.copy("*.dll", dst="bin", src="bin")
      self.copy("*.dylib*", dst="bin", src="lib")

    def build(self):
       build_path = path.join(self.build_folder, "build")

       build_cmd = 'waf configure build -v -o {} {}'.format(build_path, self.get_options())

       self.run_and_log(build_cmd, cwd=self.source_folder)
       self.run_and_log('waf install', cwd=self.source_folder)

    def package(self):
        self.copy("*.h", src=path.join(self.install_dir, "include"), dst="include", keep_path=True)
        self.copy("*", src=path.join(self.install_dir, "lib"), dst="lib", symlinks=True)

    def package_info(self):
        self.cpp_info.libs = ['PocoDNSSD']

########################################################################################################

    def get_options(self):
        opts = []

        import multiprocessing
        opts.append("-j {}".format(multiprocessing.cpu_count()))

        if self.settings.build_type == "Debug":
           opts.append("--debug")
        else:
           opts.append("--release")

        #       if self.options.shared:
        #           opts.append("--shared")

        opts.append("-v")

        opts.append("--prefix=%s" % self.install_dir)

        return " ".join(opts)

    @property
    def install_dir(self):
       return path.join(self.build_folder, "install")

    def run_and_log(self, cmd, cwd=None):
       msg = ''
       if cwd:
           msg = "[{}] ".format(cwd)

       self.output.info(msg + cmd)

       self.run(cmd, cwd = cwd)
