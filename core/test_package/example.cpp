#include "Poco/DNSSD/DNSSDResponder.h"
#include "Poco/DNSSD/DNSSDBrowser.h"

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

void run(){
    cout << "************ Running test ***********" << endl;

    cout << "Creating DNS-SD responder" << endl;
    Poco::DNSSD::DNSSDResponder dnssdResponder;

    cout << "Creating service instance" << endl;
    Poco::DNSSD::Service service("_http._tcp", 8080);

    cout << "**************************************" << endl;
}

int main(){
    try{
        run();
    }
    catch(const Poco::Exception & e){
        cerr << e.displayText() << endl;
        cerr << "Aborted" << endl;
    }
    catch(...){
        cerr << "Unexpected error!" << endl;
        throw;
    }

    return 0;
}
