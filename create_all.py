#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import sys

def main():
    if len(sys.argv) < 2:
        sys.stderr.write("missing 'user/channel' info\n")
        print("usage: {} <user/channel>".format(sys.argv[0]))
        return

    user_channel = sys.argv[1]

    for subdir in ['core', 'Avahi', 'Bonjour', '.']:
        cmd = "conan create . {}".format(user_channel)
        subprocess.call(cmd.split(), cwd=subdir)

if __name__ == "__main__":
    main()
