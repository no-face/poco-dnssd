#include "Poco/DNSSD/DNSSDResponder.h"
#include "Poco/DNSSD/DNSSDBrowser.h"
//#if POCO_OS == POCO_OS_LINUX && !defined(POCO_DNSSD_USE_BONJOUR)
#ifdef HAS_POCO_DNSSD_AVAHI
#warning "Using avahi!"
#include "Poco/DNSSD/Avahi/Avahi.h"
#else
#warning "Using Bonjour!"
#include "Poco/DNSSD/Bonjour/Bonjour.h"
#endif

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

void run(){
    cout << "************ Running test ***********" << endl;

    cout << "Initializing DNSSD implementation" << endl;
    Poco::DNSSD::initializeDNSSD();

    cout << "Starting DNS-SD responder" << endl;
    Poco::DNSSD::DNSSDResponder dnssdResponder;
    dnssdResponder.start();

    cout << "Registering service" << endl;
    Poco::DNSSD::Service service("_http._tcp", 8080);
    Poco::DNSSD::ServiceHandle serviceHandle = dnssdResponder.registerService(service);

    //In a real application, we should wait for events and check for errors

    cout << "Unregistering service" << endl;
    dnssdResponder.unregisterService(serviceHandle);

    cout << "Uninitializing DNSSD implementation" << endl;
    Poco::DNSSD::uninitializeDNSSD();

    cout << "**************************************" << endl;
}

int main(){
    try{
        run();
    }
    catch(const Poco::Exception & e){
        cerr << e.displayText() << endl;
    }
    catch(...){
        throw;
    }

    return 0;
}
