#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path

from waflib.Configure import conf

def load_tools(ctx):
    ctx.load('compiler_cxx')
    ctx.load('gnu_dirs')


def options(ctx):
    load_tools(ctx)

    ctx.add_option('--debug', action='store_true', default=True, dest='debug', help='Do debug build')
    ctx.add_option('--release', action='store_false', dest='debug', help='Do release build')

    # Flag to generate shared or static libs
    ctx.add_option('--shared', action='store_true', default=True, dest='shared', help='Build libs as shared libraries')
    ctx.add_option('--static', action='store_false', dest='shared', help='Build libs as static libraries')


    ctx.add_option('--use-avahi'  , action='store', default=False, help='Build avahi wrappers')
    ctx.add_option('--use-bonjour', action='store', default=False, help='Build bonjour (libmnds) wrappers')

def configure(ctx):
    load_tools(ctx)

    ctx.load('conanbuildinfo_waf', tooldir=[".", path.join(ctx.bldnode.abspath(), "..")]);

    ctx.env.shared      = (ctx.options.shared      == True or ctx.options.shared      == 'True')
    ctx.env.use_avahi   = (ctx.options.use_avahi   == True or ctx.options.use_avahi   == 'True')
    ctx.env.use_bonjour = (ctx.options.use_bonjour == True or ctx.options.use_bonjour == 'True')

    config_flags(ctx)

def build(ctx):
    #sources = ctx.glob('src/*.cpp')
    sources = ctx.srcnode.ant_glob('src/*.cpp')

    print("'{}': {}".format(ctx.srcnode.abspath(), ctx.srcnode.listdir()))
    print("sources: {}".format(sources))

    ctx.lib(target="PocoDNSSD"
        , source                = sources
        , includes              = ['include']
        , export_includes       = ['include']
        , headers_base          = 'include'
        , use                   = ['Poco']
    )

    print("use_avahi: {}".format(ctx.env.use_avahi))

    if ctx.env.use_avahi:
        ctx.recurse('Avahi')

    if ctx.env.use_bonjour:
        ctx.recurse('Bonjour')

#######################################################################################

def config_flags(ctx):
    if ctx.options.debug:
        ctx.env.CXXFLAGS += ['-g']

    ctx.env.CXXFLAGS += ['-std=c++11', '-fPIC', '-Wall']

    if ctx.check_cxx(cxxflags='-pthread', linkflags="-pthread"):
        # set pthread globally to solve pre-compiled header problems
        ctx.env.CXXFLAGS += ['-pthread']
        ctx.env.LINKFLAGS += ['-pthread']

    if ctx.check_cxx(linkflags="-Wl,--no-undefined"):
        ctx.env.LINKFLAGS.append("-Wl,--no-undefined")

################################### Helpers ###########################################

def try_remove(list, item):
    if item in list:
        list.remove(item)

@conf
def glob(ctx, *k, **kw):
    '''Helper to execute an ant_glob search.
        See documentation at: https://waf.io/apidocs/Node.html?#waflib.Node.Node.ant_glob
    '''

    return ctx.path.ant_glob(*k, **kw)

@conf
def lib(bld, **kw):
    import waflib
    from waflib import Utils
    from waflib.Tools import c_aliases

    if 'install_path' not in kw:
        kw['install_path'] = bld.env.LIBDIR

    features     = Utils.to_list(kw.get("features", ""))
    only_objects = kw.get("only_objects", False)

    objects_kw = kw.copy()

    if not only_objects:
        objects_target = kw.get('target') + ".objects"
        objects_kw['target'] = objects_target

    bld.objects(**objects_kw)

    if not only_objects:
        kw['use']      = [objects_target] + Utils.to_list(kw.get('use', ""))
        #kw['features'] = ["cxx"] + Utils.to_list(kw.get('features', ""))

        lib_type = "shlib" if bld.env.shared else "stlib"
        c_aliases.set_features(kw, lib_type)

        # remove source to not build the same sources again
        kw['source']   = []

        # build library
        bld(**kw)

    includedir = kw.get('install_includedir', bld.env.INCLUDEDIR)

    install_headers = []

    if 'install_headers' in kw:
        install_headers = kw['install_headers']
    else:
        headers_dirs = kw.get('headers_dirs', kw.get('export_includes', []))

        for inc_dir in headers_dirs:

            inc_node = bld.path.make_node(inc_dir)
            incs = inc_node.ant_glob([
                                path.join('**', '*.h'),
                                path.join('**', '*.hpp')])

            install_headers.extend(incs)

    if install_headers:
        headers = Utils.to_list(install_headers)

        headers_base = kw.get("headers_base", None)
        if headers_base and not isinstance(headers_base, waflib.Node.Node):
            headers_base = bld.path.find_dir(headers_base)

        relative_trick = kw.get("install_relative", True)
        bld.install_files(
            includedir,
            headers,
            relative_trick=relative_trick,
            cwd=headers_base)
